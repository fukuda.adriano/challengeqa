Project automation tests for Challenge

API testing in c# with RestSharp, Specflow and FluentAssertions.

## Dependencies

* .net Core 3.1

* https://dotnet.microsoft.com/download/dotnet/3.1

#### Run the scenarios

* Default all scenarios:

          $ dotnet test
