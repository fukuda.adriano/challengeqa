﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ApiTests.DataEntities
{
    public class BestMatch
    {
        [JsonProperty("bestMatches")]
        public List<Company> BestMatches { get; set; }
    }
}
