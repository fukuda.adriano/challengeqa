﻿Feature: Search
	The Search Endpoint returns the best-matching symbols and market information based on keywords of your choice

Scenario: Searching a company for real name keywords
	Given the name is Tencent Music Entertainment Group
	When the search is requested 
	Then the result list should contain match the name
	
Scenario: Searching a company for partial name keywords
    Given the name is Tenc
	When the search is requested 
	Then the result list should contain match the partial name

Scenario: Searching a company for real symbol keywords
	Given the symbol is FTCHX
	When the search is requested
	Then the result list should contain match the symbol

Scenario: Searching a company for partial symbol keywords
    Given the symbol is BA
	When the search is requested 
	Then the result list should contain match the partial symbol

Scenario: It doesn't exist keyword in system
	Given the keyword doesn't existent FHTC
	When the search is requested
	Then the result list should be empty


