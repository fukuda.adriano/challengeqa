﻿using RestSharp;

namespace ApiTests.Gateway
{
    public class AlphaVantage
    {
        const string url = "https://www.alphavantage.co";
        const string apiKey = "5FLHPEEZGIZ1TNSL";

        public IRestResponse GetSearchEndpoint(string keyword)
        {
            RestClient client = new RestClient(url);
            RestRequest request = new RestRequest($"query?function=SYMBOL_SEARCH&keywords={keyword}&apikey={apiKey}", Method.GET);
            IRestResponse response = client.Execute(request);
            return response;
        }
    }
}
