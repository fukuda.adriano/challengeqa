﻿using ApiTests.DataEntities;
using ApiTests.Gateway;
using FluentAssertions;
using Newtonsoft.Json;
using RestSharp;
using TechTalk.SpecFlow;

namespace QAChallenge.Steps
{
    [Binding]
    public class SearchStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;
        private readonly AlphaVantage _alphaVantage;

        public SearchStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _alphaVantage = new AlphaVantage();
        }

        [Given(@"the name is (.*)")]
        public void GivenTheNameIsTencent(string name)
        {
            _scenarioContext["keyword"] = name;
        }
        
        [Given(@"the symbol is (.*)")]
        public void GivenTheSymbolIsFHTC(string symbol)
        {
            _scenarioContext["keyword"] = symbol;
        }
        
        [When(@"the search is requested")]
        public void WhenTheSerachIsRequested()
        {
            var keyword = (string)_scenarioContext["keyword"];
            IRestResponse response = _alphaVantage.GetSearchEndpoint(keyword);
            _scenarioContext["response"] = response;
        }
        
        [Then(@"the result list should contain match the name")]
        public void ThenTheResultListShouldContainMatchTheName()
        {
            var keyword = (string)_scenarioContext["keyword"];
            IRestResponse response = (IRestResponse)_scenarioContext["response"];
            BestMatch bestMatch = JsonConvert.DeserializeObject<BestMatch>(response.Content);

            response.StatusCode.Should().Be(200);
            bestMatch.BestMatches.Should().NotBeNullOrEmpty();
            bestMatch.BestMatches.Should().HaveCount(1);
            bestMatch.BestMatches.Should().OnlyContain(x => x.Name.Equals(keyword));
        }
        
        [Then(@"the result list should contain match the partial name")]
        public void ThenTheResultListShouldContainMatchThePartialName()
        {
            var keyword = (string)_scenarioContext["keyword"];
            IRestResponse response = (IRestResponse)_scenarioContext["response"];
            BestMatch bestMatch = JsonConvert.DeserializeObject<BestMatch>(response.Content);

            response.StatusCode.Should().Be(200);
            bestMatch.BestMatches.Should().NotBeNullOrEmpty();
            bestMatch.BestMatches.Should().OnlyContain(x => x.Name.Contains(keyword));
        }
        
        [Then(@"the result list should contain match the symbol")]
        public void ThenTheResultListShouldContainMatachTheSymbol()
        {
            var keyword = (string)_scenarioContext["keyword"];
            IRestResponse response = (IRestResponse)_scenarioContext["response"];
            BestMatch bestMatch = JsonConvert.DeserializeObject<BestMatch>(response.Content);

            response.StatusCode.Should().Be(200);
            bestMatch.BestMatches.Should().NotBeNullOrEmpty();
            bestMatch.BestMatches.Should().HaveCount(1);
            bestMatch.BestMatches.Should().OnlyContain(x => x.Symbol.Equals(keyword));
        }
        
        [Then(@"the result list should contain match the partial symbol")]
        public void ThenTheResultListShouldContainMatchThePartialSymbol()
        {
            var keyword = (string)_scenarioContext["keyword"];
            IRestResponse response = (IRestResponse)_scenarioContext["response"];
            BestMatch bestMatch = JsonConvert.DeserializeObject<BestMatch>(response.Content);

            response.StatusCode.Should().Be(200);
            bestMatch.BestMatches.Should().NotBeNullOrEmpty();
            bestMatch.BestMatches.Should().OnlyContain(x => x.Symbol.Contains(keyword) || x.Name.Contains(keyword));
        }

        [Given(@"the keyword doesn't existent (.*)")]
        public void GivenTheKeywordDoesnTExistentFHTC(string keywordNonExistent)
        {
            _scenarioContext["keyword"] = keywordNonExistent;
        }

        [Then(@"the result list should be empty")]
        public void ThenTheResultListShouldBeEmpty()
        {
            var keyword = (string)_scenarioContext["keyword"];
            IRestResponse response = (IRestResponse)_scenarioContext["response"];
            BestMatch bestMatch = JsonConvert.DeserializeObject<BestMatch>(response.Content);

            response.StatusCode.Should().Be(200);
            bestMatch.BestMatches.Should().BeEmpty();
        }

    }
}
